# frozen_string_literal: true

require_relative '../lib/slo_breach_helper'

Gitlab::Triage::Resource::Context.include SloBreachHelper
