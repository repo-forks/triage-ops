# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/merge_request_coach_helper'

RSpec.describe MergeRequestCoachHelper do
  include_context 'with event', Triage::MergeRequestNoteEvent do
    let(:label_names) { ['frontend', 'group::group1'] }
    let(:processor_klass) do
      Class.new do
        include MergeRequestCoachHelper

        def event
          raise NotImplementedError
        end
      end
    end

    subject(:processor) { processor_klass.new }

    before do
      allow(processor).to receive(:event).and_return(event)
    end

    describe '#coach' do
      it 'calls select_random_merge_request_coach with expected params' do
        coach = Object.new
        allow(processor).to receive(:select_random_merge_request_coach)
          .with(group: 'Group 1', role: /frontend/).and_return(coach)

        expect(processor.send(:coach)).to eq(coach)
      end

      context 'without frontend label' do
        let(:label_names) { ['backend'] }

        it 'calls select_random_merge_request_coach with expected params' do
          coach = Object.new
          allow(processor).to receive(:select_random_merge_request_coach)
            .with(group: nil, role: nil).and_return(coach)

          expect(processor.send(:coach)).to eq(coach)
        end
      end
    end

    describe '#group' do
      it 'returns group by label' do
        expect(processor.send(:group).key).to eq('group1')
      end
    end

    describe '#role' do
      context 'when labels include frontend' do
        it 'returns regexp' do
          expect(processor.send(:role)).to eq(/frontend/)
        end
      end

      context 'when labels do not include frontend' do
        let(:label_names) { [] }

        it 'returns nil' do
          expect(processor.send(:role)).to be_nil
        end
      end
    end
  end
end
