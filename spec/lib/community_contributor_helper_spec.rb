# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/community_contribution_helper'

RSpec.describe CommunityContributionHelper do
  let(:resource_klass) do
    Struct.new(:resource, :author) do
      include CommunityContributionHelper
    end
  end

  let(:author_id) { 42 }
  let(:author_username) { 'community-member' }
  let(:resource) do
    { author: { id: author_id, username: author_username } }
  end

  subject { resource_klass.new(resource, author_username) }

  describe '#maybe_automation_author?' do
    context 'when author is not a bot' do
      it 'returns false' do
        expect(subject).not_to be_maybe_automation_author
      end
    end

    project_bot_names = %w[project_123_bot project_123_bot12]

    project_bot_names.each do |project_bot_name|
      context "when author is a project bot user #{project_bot_name}" do
        let(:author_username) { project_bot_name }

        it 'returns true' do
          expect(subject).to be_maybe_automation_author
        end
      end
    end

    Triage::Event::AUTOMATION_IDS.each do |bot_id|
      context "when author is a known bot with id #{bot_id}" do
        let(:author_id) { bot_id }

        it 'returns true' do
          expect(subject).to be_maybe_automation_author
        end
      end
    end

    context "with service account author" do
      context "when user username is a project service account" do
        let(:author_username) { 'project_278964_bot4' }

        it 'returns true' do
          expect(subject.maybe_automation_author?).to be(true)
        end
      end

      context "when user username is a group service account" do
        let(:author_username) { 'group_7137538_bot_97d0ed2e6bb4ab962e3a43a628c06974' }

        it 'returns true' do
          expect(subject.maybe_automation_author?).to be(true)
        end
      end

      context "when user username is foo" do
        let(:author_username) { 'foo' }

        it 'returns false' do
          expect(subject.maybe_automation_author?).to be(false)
        end
      end
    end
  end
end
