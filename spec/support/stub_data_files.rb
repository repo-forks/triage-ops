# frozen_string_literal: true

module StubDataFiles
  extend self

  def stub_all_data_files!
    WebMock::API.stub_request(:get, "https://about.gitlab.com/sections.json")
      .to_return(status: 200, body: ReadFixtures.read_fixture('sections.json'), headers: {})

    WebMock::API.stub_request(:get, "https://about.gitlab.com/stages.json")
      .to_return(status: 200, body: ReadFixtures.read_fixture('stages.json'), headers: {})

    WebMock::API.stub_request(:get, "https://about.gitlab.com/groups.json")
      .to_return(status: 200, body: ReadFixtures.read_fixture('groups.json'), headers: {})

    WebMock::API.stub_request(:get, "https://about.gitlab.com/categories.json")
      .to_return(status: 200, body: ReadFixtures.read_fixture('categories.json'), headers: {})

    WebMock::API.stub_request(:get, "https://about.gitlab.com/company/team/team.yml")
      .to_return(status: 200, body: ReadFixtures.read_fixture('team.yml'), headers: {})

    WebMock::API.stub_request(:get, "https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json")
      .to_return(status: 200, body: ReadFixtures.read_fixture('roulette.json'), headers: {})

    WebMock::API.stub_request(:get, "https://gitlab.com/gitlab-org/distribution/monitoring/-/raw/master/lib/data_sources/projects.yaml")
      .to_return(status: 200, body: ReadFixtures.read_fixture('distribution_projects.yml'), headers: {})

    WebMock::API.stub_request(:get, "https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/releases.yml")
      .to_return(status: 200, body: ReadFixtures.read_fixture('releases.yml'))
  end
end
