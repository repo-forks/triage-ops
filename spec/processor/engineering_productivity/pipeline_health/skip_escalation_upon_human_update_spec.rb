# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/processor/engineering_productivity/pipeline_health/skip_escalation_upon_human_update'

RSpec.describe Triage::PipelineHealth::SkipEscalationUponHumanUpdate do
  include_context 'with event', Triage::IncidentEvent do
    let(:is_master_broken_incident_project) { true }
    let(:by_team_member) { true }
    let(:resource_open) { true }
    let(:event_attrs) do
      { from_master_broken_incidents_project?: is_master_broken_incident_project,
        by_team_member?: by_team_member,
        resource_open?: resource_open,
        label_names: label_names }
    end

    let(:label_names) { ['master:broken'] }
  end

  subject(:skip_escalation_upon_human_update) { described_class.new(event) }

  include_examples 'registers listeners', ['incident.update', 'issue.note']
  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event is not from master-broken-incident project' do
      let(:is_master_broken_incident_project) { false }

      include_examples 'event is not applicable'
    end

    context 'when the event resource is closed' do
      let(:resource_open) { false }

      include_examples 'event is not applicable'
    end

    context 'when event actor is not a team member' do
      let(:by_team_member) { false }

      include_examples 'event is not applicable'
    end

    context 'when event already has escalation::skipped label' do
      let(:label_names) { ['escalation::skipped', 'master:broken'] }

      include_examples 'event is not applicable'
    end

    context 'when event already has escalation::escalated label' do
      let(:label_names) { ['escalation::escalated', 'master:broken'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = '/label ~"escalation::skipped"'

      expect_comment_request(event: event, body: body) do
        skip_escalation_upon_human_update.process
      end
    end
  end
end
