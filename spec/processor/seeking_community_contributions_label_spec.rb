# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/seeking_community_contributions_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::SeekingCommunityContributionsLabel do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'update',
        added_label_names: labels_added,
        description: issue_description,
        from_gitlab_group?: from_gitlab_group
      }
    end

    let(:from_gitlab_group) { true }
    let(:labels_added) { ['Seeking community contributions'] }

    let(:issue_description) do
      <<~DESCRIPTION
        ## Implementation plan
        This is the implementation plan
      DESCRIPTION
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.close", "issue.note", "issue.open", "issue.reopen", "issue.update"]

  describe '#applicable?' do
    where(:from_gitlab_group, :labels_added, :issue_description, :expected) do
      [
        # Behaves as expected (from gitlab org/com/community/components)
        [true, ['Seeking community contributions'], '', 'event is applicable'],

        # Event is not from gitlab group
        [false, ['Seeking community contributions'], '', 'event is not applicable'],

        # Event is not adding the Seeking community contributions label
        [true, ['frontend'], '', 'event is not applicable'],

        # Weight is correct, we check the implementation plan
        [true, ['Seeking community contributions'], 'short description', 'event is applicable'],
        [true, ['Seeking community contributions'], "# Implementation plan\n", 'event is applicable'],
        [true, ['Seeking community contributions'], "## Implementation plan\n", 'event is applicable'],
        [true, ['Seeking community contributions'], "## Implementation plan\n\n\n", 'event is applicable'],
        [true, ['Seeking community contributions'], "## Implementation plan\n# First level heading\nImplementation plan on the wrong line", 'event is applicable'],
        [true, ['Seeking community contributions'], "## Implementation plan\n## Second level heading\nImplementation plan on the wrong line", 'event is applicable'],
        [true, ['Seeking community contributions'], "## Implementation plan\n### Third level heading", 'event is applicable'],
        [true, ['Seeking community contributions'], "### Implementation plan\n", 'event is applicable'],

        # Third level heading with content or extra headers
        [true, ['Seeking community contributions'], "## Implementation plan\n### Third level heading\nThis is the implementation plan", 'event is not applicable'],
        [true, ['Seeking community contributions'], "## Implementation plan\n### Third level heading\nThis is the implementation plan\n## Unrelated heading", 'event is not applicable'],
        [true, ['Seeking community contributions'], "## Implementation abc\nThis is the implementation plan", 'event is not applicable'],
        [true, ['Seeking community contributions'], "## Implementation\nThis is the implementation plan", 'event is not applicable']
      ]
    end

    with_them do
      it 'behaves as expected' do
        expect(subject).not_to be_applicable if expected == 'event is not applicable'
        expect(subject).to be_applicable if expected == 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts implementation guide comment' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          @root thanks for adding the ~"Seeking community contributions" label!

          This issue's description does not seem to have an Implementation plan heading
          with guidance to help contributors get started.
          For example ## Implementation, ### Implementation, ## Implementation plan and ### Implementation guide are all acceptable.
          This section can be very brief or offer possible actions to resolve the issue. Please reach out in #mr-coaching in Slack (internal) or #contribute in Discord if you need assistance.

          Please consider adding one, because it makes a [big difference for contributors](https://handbook.gitlab.com/handbook/engineering/development/dev/create/remote-development/community-contributions/#treat-wider-community-as-primary-audience).
          This section can be brief but must have clear technical guidance, like:

          - Hints on lines of code which may need changing
          - Hints on similar code/patterns that can be leveraged
          - Suggestions for test coverage
          - Ideas for breaking up the merge requests into iterative chunks
          - Links to documentation (within GitLab or external) about implementation or testing guidelines, especially when working with third-party libraries

          Need help? Reach out to the [Contributor Success](https://about.gitlab.com/handbook/marketing/developer-relations/contributor-success/) team in #contributor-success, or in [Discord](https://discord.gg/gitlab).
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
