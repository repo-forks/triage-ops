# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/triage/handler'

RSpec.describe Triage::Handler do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      { object_kind: Triage::Event::ISSUE,
        note?: false }
    end
  end

  let(:processor1) do
    Class.new(Triage::Processor) do
      react_to 'issue.*'

      def self.name
        'processor1'
      end

      def process
        true
      end
    end
  end

  let(:processor2) do
    Class.new(Triage::Processor) do
      react_to 'merge_request.note'

      def self.name
        'processor2'
      end

      def process
        nil
      end
    end
  end

  let(:processor3) do
    Class.new(Triage::Processor) do
      react_to '*.*'

      def self.name
        'processor3 - comment cleanup applicable'
      end

      def applicable?
        false
      end

      def comment_cleanup_applicable?
        true
      end

      def process
        'processed'
      end

      def processor_comment_cleanup
        'cleaned up'
      end
    end
  end

  let(:processor4) do
    Class.new(Triage::Processor) do
      react_to '*.*'

      def self.name
        'processor not applicable'
      end

      def applicable?
        false
      end

      def comment_cleanup_applicable?
        false
      end

      def process
        'processed'
      end

      def processor_comment_cleanup
        'cleaned up'
      end
    end
  end

  subject { described_class.new(event, processors: [processor1, processor2, processor3, processor4]) }

  describe 'DEFAULT_PROCESSORS' do
    it 'includes all processor implementations' do
      expected = [
        Triage::ApplyLabelsFromRelatedIssue,
        Triage::AssignDevForVerification,
        Triage::AvailabilityPriority,
        Triage::BreakingChangeComment,
        Triage::BrokenMasterLabelNudger,
        Triage::CeoChiefOfStaffTeamOpsNotifier,
        Triage::CiComponentsLabelProcessor,
        Triage::CopySecurityIssueLabels,
        Triage::CustomerLabel,
        Triage::DefaultLabelUponClosing,
        Triage::DocsOnlyLabeller,
        Triage::EngineeringAllocationLabelsReminder,
        Triage::HandbookMaintainerOpsNotifier,
        Triage::LabelInference,
        Triage::LabelJiHuContribution,
        Triage::LegalDisclaimerOnDirectionResources,
        Triage::MrApprovedLabel,
        Triage::NudgeForMissingAutoMerge,
        Triage::PajamasMissingWorkflowLabelOrWeight,
        Triage::PipelineTierTransitions,
        Triage::ProdOpsFlowNotifier,
        Triage::QuickWinLabel,
        Triage::QuickWinFirstTimeContributorLabel,
        Triage::ReactToChanges,
        Triage::RemindMergedMrDeviatingFromGuideline,
        Triage::RemoveRunE2eOmnibusOnceLabel,
        Triage::RequireTypeOnRefinement,
        Triage::RevertMrTemplateNudger,
        Triage::ScheduledIssueTypeLabelNudger,
        Triage::SeekingCommunityContributionsLabel,
        Triage::StableE2eCommentOnApproval,
        Triage::TeamLabelInference,
        Triage::TrackOrganizationContributions,
        Triage::TypeLabelNudger,
        Triage::UxPaperCutsMrs,

        # GitLab Terraform Provider processors
        Triage::NewPipelineOnApproval,

        # GitLab internal commands
        Triage::CommandRetryPipelineOrJob,
        Triage::CommandDeleteBotComment,

        # AppSec processors
        Triage::ApprovedByAppSec,
        Triage::AppSecApprovalLabelAdded,
        Triage::PingAppSecOnApproval,
        Triage::RevokeAppSecApproval,
        Triage::CommentOnPublicMRReferencingVulnerability,

        # Community processors
        Triage::AutomatedReviewRequestDoc,
        Triage::AutomatedReviewRequestGeneric,
        Triage::AutomatedReviewRequestUx,
        Triage::CodeReviewExperienceFeedback,
        Triage::CommandIssueHelp,
        Triage::CommandMrFeedback,
        Triage::CommandMrHandoff,
        Triage::CommandMrHelp,
        Triage::CommandMrLabel,
        Triage::CommandMrRequestReview,
        Triage::CommandMrUnassignReview,
        Triage::CommonRoom,
        Triage::DetectAndFlagSpam,
        Triage::FailedPipelineHelp,
        Triage::GrowthAffectingNotifier,
        Triage::HackathonLabel,
        Triage::LabelLeadingOrganization,
        Triage::RemoveIdleLabelOnActivity,
        Triage::ResetReviewState,
        Triage::ThankContribution,

        # Database processors
        Triage::DatabaseReviewExperienceFeedback,

        # Delivery processors
        Triage::SecuritySyncApprove,
        Triage::VerifyBotApprovals,

        # Engineering Productivity processors
        Triage::AddQuarantineLabelsToRelatedIssue,
        Triage::MonitoringEventProcessor,
        Triage::GdkIssuableLabel,
        Triage::PipelineHealth::PipelineFailureManagement,
        Triage::PipelineHealth::PipelineIncidentSlaEnforcer,
        Triage::PipelineHealth::SkipEscalationUponHumanUpdate,
        Triage::PipelineHealth::UpdateMasterPipelineJobsStatus,

        # Govern:Compliance processors
        Triage::AssignComplianceDevForVerification,

        # Security Insights processors
        Triage::TagAuthorIfReadyForDevWithoutWeight,

        # Support team processors
        Triage::SupportTeamContributions,
        Triage::RequestForHelpLifecycle,

        Triage::Workflow::ClosedIssueWorkflowLabelUpdater,
        Triage::Workflow::CompleteWhenMrClosesIssue,
        Triage::Workflow::DevopsLabelsNudger,
        Triage::Workflow::InfradevIssueLabelNudger,
        Triage::Workflow::NotifyMergedMasterBrokenMergeRequest,
        # Temporarily disabled see https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/382
        # Triage::Workflow::NewIssueWorkflowLabeller,

        # Growth team processors
        Triage::AddThreadOnRefinement,
        Triage::SetMilestoneOnDev,

        # Docs team processors
        Triage::Docs::TechnicalWritingLabelApproved,
        Triage::Docs::TechnicalWritingLabelMerged,

        # Privacy team processors
        Triage::DeniedDataSubjectRequest,

        # Analytics instrumentation processors
        Triage::AnalyticsInstrumentationExperienceFeedback,

        # Optimize Group processor
        Triage::FeatureMrSeedingScriptReminder
      ]

      expect(described_class::DEFAULT_PROCESSORS).to match_array(expected)
    end

    described_class::DEFAULT_PROCESSORS.each do |processor|
      it "reacts to something for #{processor}" do
        expect(processor.listeners).to be_any
      end
    end
  end

  describe '#process' do
    it 'executes processors that listen to the event' do
      expect(processor1).to receive(:triage).once.and_call_original
      expect(processor2).not_to receive(:triage)
      expect(processor3).to receive(:triage).once.and_call_original
      expect(processor4).to receive(:triage).once.and_call_original

      results = subject.process

      expect(results[processor1.name].message).to be true
      expect(results[processor1.name].executed_method).to eq('process')
      expect(results[processor2.name]).to be_nil
      expect(results[processor3.name].message).to be 'cleaned up'
      expect(results[processor3.name].executed_method).to eq('processor_comment_cleanup')
      expect(results[processor4.name]).to be_nil
    end

    context 'when a processor raises an error' do
      let(:error) { RuntimeError.new }

      before do
        allow(processor1).to receive(:triage).and_raise(error)
      end

      it 'captures error' do
        results = subject.process

        expect(results[processor1.name].error).to eq(error)
        expect(results[processor2.name]).to be_nil
        expect(results[processor3.name].error).to be_nil
      end

      it 'sends error to Sentry' do
        expect(Raven).to receive(:capture_exception).with(error)

        subject.process
      end

      it 'executes subsequent processor' do
        expect(processor2).not_to receive(:triage)
        expect(processor3).to receive(:triage).once

        subject.process
      end
    end
  end
end
