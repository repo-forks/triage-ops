# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/telemetry'
require_relative '../../triage/triage/event'

RSpec.describe Triage::Telemetry do
  include_context 'with event', Triage::PipelineEvent do
    let(:event_attrs) do
      {
        id: '42',
        event_actor_username: event_actor_username,
        web_url: 'pipeline_web_url'
      }
    end

    let(:event_actor_username) { 'root' }
  end

  describe '.record_processor_execution' do
    let(:sdk_app_id) { 'abc-123' }
    let(:client) { double('Client') } # rubocop:todo RSpec/VerifiedDoubles
    let(:processor) { Class.new }

    before do
      stub_env('GITLAB_SDK_APP_ID', sdk_app_id)

      allow(described_class).to receive(:record_processor_execution).and_call_original

      allow(described_class).to receive_messages(client: client)
    end

    context 'when telemetry is not enabled' do
      let(:sdk_app_id) { nil }

      it 'does not track telemetry and directly yields the block' do
        expect { |b| described_class.record_processor_execution(event: event, processor: processor, &b) }.to yield_control
      end
    end

    context 'when the return value is nil' do
      it 'does not track telemetry and returns nil' do
        return_value = nil
        expect(client).not_to receive(:identify)
        expect(client).not_to receive(:track)

        expect(described_class.record_processor_execution(event: event, processor: processor) { return_value }).to eq(return_value)
      end
    end

    it 'tracks the finish of the command' do
      return_value = true
      expect(client).to receive(:identify).with(event_actor_username)
      expect(client).to receive(:track).with(processor.class.to_s, hash_including(:duration))

      expect(described_class.record_processor_execution(event: event, processor: processor) { return_value }).to eq(return_value)
    end
  end

  describe '.client' do
    before do
      described_class.instance_variable_set(:@client, nil)

      stub_env('GITLAB_SDK_APP_ID', 'app_id')
      stub_env('GITLAB_SDK_HOST', 'https://collector')

      allow(GitlabSDK::Client).to receive_messages(new: mocked_client)
    end

    after do
      described_class.instance_variable_set(:@client, nil)
    end

    let(:mocked_client) { instance_double(GitlabSDK::Client) }

    it 'initializes the gitlab sdk client with the correct configuration' do
      expect(SnowplowTracker::LOGGER).to receive(:level=).with(Logger::WARN)
      expect(GitlabSDK::Client).to receive(:new).with(app_id: 'app_id', host: 'https://collector').and_return(mocked_client)

      described_class.send(:client)
    end

    context 'when client is already initialized' do
      before do
        described_class.instance_variable_set(:@client, mocked_client)
      end

      it 'returns the existing client without reinitializing' do
        expect(GitlabSDK::Client).not_to receive(:new)
        expect(described_class.send(:client)).to eq(mocked_client)
      end
    end
  end
end
