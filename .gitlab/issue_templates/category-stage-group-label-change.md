## Summary

_Specify which resources you would like to target and the expected result after the migration._

CHANGEME

### Action items for issue author
* [ ] Provide link to the merge request in [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) for the label change.
* [ ] Identify any necessary code update in this repo because of this label change. Tip: search for a label that should be removed/updated in the [triage-ops repository](https://gitlab.com/gitlab-org/quality/triage-ops).
* [ ] (If applicable) Self service one-off label migration:
  * [ ] Refer to the [handbook page](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/developer-experience/development-analytics/create-triage-policy-with-gitlab-duo-workflow-guide) for instructions on how to get started with this MR with the help of GitLab Duo Workflow.
* [ ] (If applicable) Archive the old label with renaming and adding "DEPRECATED" at the end of the label name.
* [ ] (If applicable) Update the label's description with link to the updated handbook page.

### Action items for Development Analytics

* [ ] Provide guidance for the label migration process when requested.
* [ ] Address any feedback for the label migration documentation.

/cc @gl-dx/development-analytics
/label ~"group::development analytics" ~"label change"
