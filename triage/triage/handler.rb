# frozen_string_literal: true

require_relative 'listener'

Dir[File.expand_path("../processor/**/*.rb", __dir__)].each { |f| require f }

module Triage
  class Handler
    DEFAULT_PROCESSORS = [
      ApplyLabelsFromRelatedIssue,
      AssignDevForVerification,
      AvailabilityPriority,
      BreakingChangeComment,
      BrokenMasterLabelNudger,
      CeoChiefOfStaffTeamOpsNotifier,
      CiComponentsLabelProcessor,
      CopySecurityIssueLabels,
      CustomerLabel,
      DefaultLabelUponClosing,
      DocsOnlyLabeller,
      EngineeringAllocationLabelsReminder,
      HandbookMaintainerOpsNotifier,
      LabelInference,
      LabelJiHuContribution,
      LegalDisclaimerOnDirectionResources,
      MrApprovedLabel,
      NudgeForMissingAutoMerge,
      PajamasMissingWorkflowLabelOrWeight,
      PipelineTierTransitions,
      ProdOpsFlowNotifier,
      QuickWinLabel,
      QuickWinFirstTimeContributorLabel,
      ReactToChanges,
      RemindMergedMrDeviatingFromGuideline,
      RemoveRunE2eOmnibusOnceLabel,
      RequireTypeOnRefinement,
      RevertMrTemplateNudger,
      ScheduledIssueTypeLabelNudger,
      SeekingCommunityContributionsLabel,
      StableE2eCommentOnApproval,
      TeamLabelInference,
      TrackOrganizationContributions,
      TypeLabelNudger,
      UxPaperCutsMrs,

      # GitLab Terraform Provider processors
      NewPipelineOnApproval,

      # Internal gitlab-bot command
      CommandRetryPipelineOrJob,
      CommandDeleteBotComment,

      # AppSec processors
      ApprovedByAppSec,
      AppSecApprovalLabelAdded,
      PingAppSecOnApproval,
      RevokeAppSecApproval,
      CommentOnPublicMRReferencingVulnerability,

      # Community processors
      AutomatedReviewRequestDoc,
      AutomatedReviewRequestGeneric,
      AutomatedReviewRequestUx,
      CodeReviewExperienceFeedback,
      CommandIssueHelp,
      CommandMrFeedback,
      CommandMrHandoff,
      CommandMrHelp,
      CommandMrLabel,
      CommandMrRequestReview,
      CommandMrUnassignReview,
      DetectAndFlagSpam,
      FailedPipelineHelp,
      GrowthAffectingNotifier,
      HackathonLabel,
      LabelLeadingOrganization,
      RemoveIdleLabelOnActivity,
      ResetReviewState,
      ThankContribution,
      CommonRoom,

      # Database processors
      DatabaseReviewExperienceFeedback,

      # Delivery processors
      SecuritySyncApprove,
      VerifyBotApprovals,

      # Engineering Productivity processors
      AddQuarantineLabelsToRelatedIssue,
      MonitoringEventProcessor,
      GdkIssuableLabel,
      PipelineHealth::PipelineFailureManagement,
      PipelineHealth::PipelineIncidentSlaEnforcer,
      PipelineHealth::SkipEscalationUponHumanUpdate,
      PipelineHealth::UpdateMasterPipelineJobsStatus,

      # Security Insights processors
      TagAuthorIfReadyForDevWithoutWeight,

      # Govern:Compliance processors
      AssignComplianceDevForVerification,

      # Support processors
      SupportTeamContributions,
      RequestForHelpLifecycle,

      Workflow::ClosedIssueWorkflowLabelUpdater,
      Workflow::CompleteWhenMrClosesIssue,
      Workflow::DevopsLabelsNudger,
      Workflow::InfradevIssueLabelNudger,
      Workflow::NotifyMergedMasterBrokenMergeRequest,
      # Temporarily disabled pending decisions from https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/382
      # Workflow::NewIssueWorkflowLabeller,

      # Growth team processors
      AddThreadOnRefinement,
      SetMilestoneOnDev,

      # Docs team processors
      Docs::TechnicalWritingLabelApproved,
      Docs::TechnicalWritingLabelMerged,

      # Privacy team processors
      DeniedDataSubjectRequest,

      # Analytics Instrumentation team processors
      AnalyticsInstrumentationExperienceFeedback,

      # Optimize Group processor
      FeatureMrSeedingScriptReminder
    ].freeze

    Result = Struct.new(:executed_method, :message, :error, :duration)

    def initialize(event, processors: DEFAULT_PROCESSORS)
      @event = event
      @processors = processors
    end

    def process
      results = Hash.new { |h, k| h[k] = Result.new }

      listeners[event.key].each do |processor|
        start_time = Triage.current_monotonic_time

        triage_result = processor.triage(event)
        results[processor.name].executed_method = triage_result[:executed_method]
        results[processor.name].message = triage_result[:return_value]
      rescue StandardError => e
        results[processor.name].error = e
        Raven.capture_exception(e)
      ensure
        results[processor.name].duration = (Triage.current_monotonic_time - start_time).round(5)
      end

      results.select { |processor, result| result.message || result.error }
    end

    private

    attr_reader :event, :processors

    def listeners
      @listeners ||= processors.each_with_object(Hash.new { |h, k| h[k] = [] }) do |processor, result|
        processor.listeners.each do |listener|
          result[listener.event] << processor
        end
      end
    end
  end
end
