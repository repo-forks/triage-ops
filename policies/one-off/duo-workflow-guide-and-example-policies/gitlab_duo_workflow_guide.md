This file is for GitLab Duo Workflow to parse before composing triage policies.

Detailed instructions for writing triage policy can be found in https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#defining-a-policy.

Here is a list of common conditions to choose from:

## date

```yaml
conditions:
  date:
    attribute: updated_at
    condition: older_than
    interval_type: months
    interval: 12
```

## milestone

```yaml
conditions:
  milestone: v1
```

## state

```yaml
conditions:
  state: opened
```

## labels

```yaml
conditions:
  labels:
    - feature proposal
```

## forbidden_labels

```yaml
conditions:
  forbidden_labels:
    - awaiting feedback
```

## no_additional_labels

```yaml
conditions:
  labels:
    - feature proposal
  no_additional_labels: true
```

## issue_type

```yaml
conditions:
  issue_type: issue
```

## ruby

```yaml
conditions:
  ruby: !resource[:confidential]
```

or

```yaml
conditions:
  ruby: |
    resource[:closed_at] > 7.days.ago.strftime('%Y-%m-%dT00:00:00.000Z')
```

And here is a list of common actions to choose from:

## labels

```yaml
actions:
  labels:
    - feature proposal
    - awaiting feedback
```

## remove_labels

```yaml
actions:
  remove_labels:
    - feature proposal
    - awaiting feedback

```

## status

```yaml
actions:
  status: close
```

## comment

Accepts a string, and placeholders. Placeholders should be wrapped in double curly braces, e.g. {{author}}.

The following placeholders are supported:

- created_at
- updated_at
- closed_at
- merged_at
- state
- author
- assignee
- labels
- title
- web_url
- type

```yaml
actions:
  comment: |
    {{author}} Are you still interested in finishing this merge request?
```

IMPORTANT: All yaml files should end with a new line!
